#ifndef TOTPSLOT_H
#define TOTPSLOT_H


/**
 * @brief TOTPSlot represents indivitual TOTP slot
 * in <a href="https://puri.sm/products/librem-key/">
 * LibremKey</a> USB key.
 *
 * TODO: Class will be implemented when GUI will be implemented.
 */
class TOTPSlot
{
public:
    /**
     * @brief Default constructor.
     */
    TOTPSlot();
};

#endif // TOTPSLOT_H
