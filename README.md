# LibremKey Authenticator
A TOTP authenticator application, that is a drop-in relplacement for
[Google Authenticator](https://www.google-authenticator.com/),
[Authy's](https://authy.com/) TOTP authentication feature and alike.
The significant difference between this applications and the ones mentioned
above is that the secret, that the server generates for you and embeds into
the QR code, that you scan with your favorite Authenticator app, is stored
in the memory of your [LibremKey](https://puri.sm/products/librem-key/)
USB stick.

## Viable alternative: Yubico Authenticator
In principle this application in very similar to
[Yubico Authenticator](https://www.yubico.com/products/services-software/download/yubico-authenticator/),
that stores the secrets in your [Yubikey](https://www.yubico.com/).

### Caveats
Yubico Authenticator is great software and I've used it a lot. But it's a
proprietary software that works with proprietary hardware. You can't know
everything, that the authenticator app is doing. Nor can you audit Yubikey's
firmware. If you truly care about openess of the software, you're running,
you're out of luck with Yubico Authenticator.

Yubico Authenticator supports couple of plaftorms, you might care about:

* Windows
* GNU/Linux
* MacOS
* Android
* iOS

If you prefer BSD-family open-source system, you're out of luck.

# Installing and using
It's early into development process, so you're not installing it system-wide.
You only get and compile the source code.

## Install prerequisites
If you're using Debian or any of its derivatives (including
[PureOS](https://www.pureos.net/), the following will pull in all
compile-time and run-time dependencies:
```bash
$ sudo apt install -y build-essential cmake git pkg-config libgtest-dev qtbase5-dev qttools5-dev libzbarqt-dev libhidapi-dev
```

## Getting the source code and compiling
```bash
$ cd
$ mkdir libremkey_authenticator_building
$ cd libremkey_authenticator_building
$ git clone https://salsa.debian.org/patryk/libremkeyauthenticator.git
$ mkdir build
$ cd build
$ cmake ../libremkeyauthenticator
$ make
```

And you can run it now:
```bask
$ ./LibremKeyAuthenticator
```

## Hacking
For testing we're using [GoogleTest](https://github.com/google/googletest). We're documenting APIs
using [Doxygen](http://doxygen.nl/). We also have a simple
[pipeline](https://salsa.debian.org/patryk/libremkeyauthenticator/-/blob/master/.gitlab-ci.yml), that
build the code and runs unit tests. Look [here](https://salsa.debian.org/patryk/libremkeyauthenticator/pipelines/137696)
for an example execution of the pipeline. There are 2 stages: `build_debian` and `test_debian`.

All unit tests are contained within `tests` target. If you want to run tests against actual USB key, put them within
`dontAddToCTest` target -- it will not be executed automatically by the pipeline. You can run it, however, to test
any LibremKey integration. Bear in mind, however, that those tests will mess up any TOTP slots, you might have on your key.
Thus, don't run it against the key, you use for your authentication. Better to have a separate spare key for development
and testing.

Typical workflow, after running `cmake`, is:
```bask
$ # Modify the code
$ make
$ ctest ../libremkeyauthenticator
$ # Repeat
```

