#include <chrono>
#include <iostream>
#include <memory>

#include <QApplication>
#include <QDateTime>

#include "libnitrokey/NitrokeyManager.h"

#include "libremkeyimpl.h"
#include "mainwindow.h"
#include "totpuriparser.h"

/**
 * @file
 * LibremKey Authenticator application
 *
 * @mainpage
 *
 * @section What is LibremKey Authenticator
 * LibremKey Authenticator is a drop-in replacement for
 * authenticator applications like
 * <a href="https://www.google-authenticator.com/">
 * Google Authenticator</a>, <a href="https://authy.com/">
 * Authy</a> and alike. The significant difference between
 * LibremKey Authenticator and others is, that the secret
 * used to generate 1-time authentication codes is stored
 * inside <a href="https://puri.sm/products/librem-key/">
 * LibremKey</a> (inside the USB key). This secret, once
 * stored inside the key, never leaves it again. Whenever
 * you're generating the code, the application requests
 * the key to generate it. The cryptographic operation of
 * generating the code is executed by the key.
 *
 * Another words, compromising your system would not be enough
 * to steal the secret(s) used for 2nd-factor-authentication.
 * Attacker would also have to hack LibremKey.
 */

void alternate() {
    auto manager = nitrokey::NitrokeyManager::instance();
    manager->set_loglevel(5);
    auto isConnected = manager->connect();
    std::cout << "isConnected: " << isConnected << std::endl;
    auto config = manager->read_config();
    for (auto &i : config) {
        std::cout << i << ", ";
    }
    auto auth = manager->first_authenticate("1234567890", "123456789");
    std::cout << auth << std::endl;
    manager->set_time_soft(QDateTime::currentDateTimeUtc().toTime_t());
    auto a = manager->get_hotp_slot_name(1);
    std::cout << "slot name: " << a << std::endl;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w(std::make_unique<LibremKeyImpl>(5));
    w.show();
    a.setQuitOnLastWindowClosed(true);
    a.exec();

    // TODO: Move to tests after integrating GoogleTest
    QString original = "otpauth://totp/patryk%40cisek.email?secret=3GWM5TLNRHDKQEPLA26MTP4JQOJPENYV&issuer=TOTP%20Test%20Application";

    TotpUriParser parser(original);

}
