#include "libremkeyimpl.h"

#include "libnitrokey/NitrokeyManager.h"

#include <QDebug>

LibremKeyImpl::LibremKeyImpl(int logLevel)
    : keyManager{nitrokey::NitrokeyManager::instance()}
{
    keyManager->set_loglevel(logLevel);
}

bool LibremKeyImpl::connect()
{
    return keyManager->connect();
}

bool LibremKeyImpl::isConnected() const
{
    return keyManager->is_connected();
}

bool LibremKeyImpl::firstAuth(const std::string &adminPin, const std::string tempPassword)
{
    return keyManager->first_authenticate(adminPin.c_str(), tempPassword.c_str());
}

void LibremKeyImpl::userAuth(const std::string &userPin, const std::string &tempPassword)
{

}

void LibremKeyImpl::writeTotpSlot(std::uint8_t slotNumber, const std::string &slotName,
                           const std::string &hexSecret, std::uint16_t timeWindow,
                           const std::string &temporaryPassword)
{
    constexpr bool USE_8_DIGITS = false;
    constexpr bool USE_ENTER = false;
    constexpr bool USE_TOKEN_ID = false;
    constexpr char *TOKEN_ID = nullptr;

    keyManager->write_TOTP_slot(slotNumber, slotName.c_str(), hexSecret.c_str(),
                                30, USE_8_DIGITS, USE_ENTER, USE_TOKEN_ID,
                                TOKEN_ID, temporaryPassword.c_str());
}

std::vector<TOTPSlot> LibremKeyImpl::getSlots() const
{
    return std::vector<TOTPSlot>();
}

std::string LibremKeyImpl::getSlotName(std::uint8_t slot) const
{
    // Internally libnitrokey uses strndup() (see strndup(3) for details)
    // to duplicate the name and returns this duplicated buffer to the
    // caller, thus we have to make sure, we're freeing the buffer using
    // free() (see free(3) for details) when no longer needed.
    auto slotNameCStr =
            std::unique_ptr<char, std::function<void(char*)>>
            (keyManager->get_totp_slot_name(slot), [](char* c) {
        std::free(c);
    });

    qDebug() << "Slot number: " << slot << ", name: " << slotNameCStr.get();
    qDebug() << "Slot name length: " << std::string(slotNameCStr.get()).size();
    return std::string(slotNameCStr.get());
}
