#include <memory>
#include "gtest/gtest.h"
#include "libremkeyimpl.h"

// Test vector:
// Base32: VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5
// HEX: AD67573C1248DE306007DD26CAEF65B2A237491D
// Online: https://www.tomeko.net/online_tools/base32.php?lang=en

class LibremKeyImplTest : public ::testing::Test
{
protected:
    std::unique_ptr<LibremKeyImpl> key;
    const std::string ADMIN_PIN = "12345678";
    const std::string USER_PIN = "123456";
    const std::string TEMP_PASSWORD = "abcdefghijk";

    const std::string B32_SECRET = "VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5";
    const std::string HEX_SECRET = "AD67573C1248DE306007DD26CAEF65B2A237491D";

    const std::string SLOT0_NAME = "test1";

    void SetUp() override
    {
        key.reset(new LibremKeyImpl(5));
    }
};

TEST_F(LibremKeyImplTest, CanConnectToLibremKey)
{
    // When
    auto isConnected = key->connect();
    std::cout << "isConnected: " << isConnected << std::endl;
    isConnected = key->isConnected();

    // Then
    ASSERT_TRUE(isConnected);
}

TEST_F(LibremKeyImplTest, CanReadTOTPSlotName)
{
    // Given
    key->connect();
    ASSERT_TRUE(key->firstAuth(ADMIN_PIN, TEMP_PASSWORD));
    key->writeTotpSlot(0, SLOT0_NAME, HEX_SECRET,
                       30, TEMP_PASSWORD);

    // When
    auto slotName = key->getSlotName(0);

    // Then
    const auto expectedName = std::string(SLOT0_NAME);
    ASSERT_EQ(expectedName, slotName);
}
