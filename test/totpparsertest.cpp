﻿#include "gtest/gtest.h"

#include <QString>
#include "totpuriparser.h"

// TODO: Implement tests for TotpUriParser class here.
TEST(TotpUrlParserTest, CorrectSecretIsReturned) {
    // Given
    const QString EXPECTED_B32_SECRET = "VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5";

    const QString VALID_QR_CODE_URL =
            "otpauth://totp/patryk%40cisek.email?"
            "secret=VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5"
            "&issuer=TOTP%20Test%20Application";
    TotpUriParser parser(VALID_QR_CODE_URL);

    // When
    const auto B32_SECRET = parser.getSecret();

    // Then
    ASSERT_EQ(EXPECTED_B32_SECRET, B32_SECRET);
}

TEST(TotpUrlParserTest, CorrectIssuerIsReturned) {
    // Given
    const QString EXPECTED_ISSUER = "TOTP Test Application";

    const QString VALID_QR_CODE_URL =
            "otpauth://totp/patryk%40cisek.email?"
            "secret=VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5"
            "&issuer=TOTP%20Test%20Application";
    TotpUriParser parser(VALID_QR_CODE_URL);

    // When
    const auto ISSUER = parser.getIssuer();

    // Then
    ASSERT_EQ(EXPECTED_ISSUER, ISSUER);
}

TEST(TotpUrlParserTest, CorrectAccountNameIsReturned) {
    // Given
    const QString EXPECTED_ACCOUNT_NAME = "patryk@cisek.email";

    const QString VALID_QR_CODE_URL =
            "otpauth://totp/patryk%40cisek.email?"
            "secret=VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5"
            "&issuer=TOTP%20Test%20Application";
    TotpUriParser parser(VALID_QR_CODE_URL);

    // When
    const auto ACCOUNT_NAME = parser.getAccountName();

    // Then
    ASSERT_EQ(EXPECTED_ACCOUNT_NAME, ACCOUNT_NAME);
}

TEST(TotpUrlParserTest, NoSecretInUri) {
    // Given
    const QString VALID_QR_CODE_URL =
            "otpauth://totp/patryk%40cisek.email?"
            "issuer=TOTP%20Test%20Application";

    EXPECT_THROW({
        try {
            // When
            TotpUriParser parser(VALID_QR_CODE_URL);

        } catch(const std::runtime_error& e) {
            // Then
            ASSERT_STREQ("NO secret!", e.what());
            throw;
        }
    }, std::runtime_error);
}

TEST(TotpUrlParserTest, NoIssuerInUri) {
    // Given
    const QString VALID_QR_CODE_URL =
            "otpauth://totp/patryk%40cisek.email?"
            "secret=VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5";

    EXPECT_THROW({
        try {
            // When
            TotpUriParser parser(VALID_QR_CODE_URL);

        } catch(const std::runtime_error& e) {
            // Then
            ASSERT_STREQ("NO issuer!", e.what());
            throw;
        }
    }, std::runtime_error);
}

TEST(TotpUrlParserTest, NoQueryInUri) {
    // Given
    const QString VALID_QR_CODE_URL =
            "otpauth://totp/patryk%40cisek.email?";

    EXPECT_THROW({
        try {
            // When
            TotpUriParser parser(VALID_QR_CODE_URL);

        } catch(const std::runtime_error& e) {
            // Then
            ASSERT_STREQ("Empty query!", e.what());
            throw;
        }
    }, std::runtime_error);
}

TEST(TotpUrlParserTest, EmptyUrl) {
    // Given
    const QString VALID_QR_CODE_URL = "";

    EXPECT_THROW({
        try {
            // When
            TotpUriParser parser(VALID_QR_CODE_URL);

        } catch(const std::runtime_error& e) {
            // Then
            ASSERT_STREQ("Empty url!", e.what());
            throw;
        }
    }, std::runtime_error);
}

TEST(TotpUrlParserTest, NoAccountNameInUri) {
    // Given
    const QString VALID_QR_CODE_URL =
            "otpauth://totp/?"
            "secret=VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5"
            "&issuer=TOTP%20Test%20Application";

    EXPECT_THROW({
        try {
            // When
            TotpUriParser parser(VALID_QR_CODE_URL);

        } catch(const std::runtime_error& e) {
            // Then
            ASSERT_STREQ("NO user!", e.what());
            throw;
        }
    }, std::runtime_error);
}

TEST(TotpUrlParserTest, WrongScheme) {
    // Given
    const QString VALID_QR_CODE_URL =
            "otpauth://patryk/patryk%40cisek.email?"
            "secret=VVTVOPASJDPDAYAH3UTMV33FWKRDOSI5"
            "&issuer=TOTP%20Test%20Application";

    EXPECT_THROW({
        try {
            // When
            TotpUriParser parser(VALID_QR_CODE_URL);

        } catch(const std::runtime_error& e) {
            // Then
            ASSERT_STREQ("Wrong sheme!", e.what());
            throw;
        }
    }, std::runtime_error);
}
