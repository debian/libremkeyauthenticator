#ifndef TOTPURIPARSER_H
#define TOTPURIPARSER_H

#include <QString>

class TotpUriParser
{
public:
    TotpUriParser(const QString &original);
    QString getAccountName();
    QString getSecret();
    QString getIssuer();

private:
    QString user;
    QString secret;
    QString issuer;
    QString query;
    void validateUrl(QString);
    void extractQuery(QString);
    void extractAccountName(QString);
    void validateQuery();
    void extractQueryValues();
};

#endif // TOTPURIPARSER_H
