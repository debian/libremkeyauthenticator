#include <iostream>

#include <QGuiApplication>
#include <QtDebug>
#include <QImage>
#include <QPoint>
#include <QRect>
#include <QWindow>
#include <QScreen>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "libremkeybase.h"

MainWindow::MainWindow(std::unique_ptr<LibremKeyBase> libremKey, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    libremKey{std::move(libremKey)},
    scanner()
{
    scanner.setAcceptDrops(true);
    connect(&scanner, &zbar::QZBar::decodedText, this, &MainWindow::onDecodedText);
    connect(&scanner, &zbar::QZBar::decoded, this, &MainWindow::onDecoded);

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onScreenshotClicked()
{
    qDebug() << "Clicked";
    auto screen = QGuiApplication::primaryScreen();
    auto screenshotPixmap = screen->grabWindow(0);
    QImage screenshotImage = screenshotPixmap.toImage();

    scanner.scanImage(screenshotImage);
}

void MainWindow::onDecodedText(const QString &text)
{
    qDebug() << "Decoded text: " << text;
}

void MainWindow::onDecoded()
{
    qDebug() << "Decoded, no text";
}
