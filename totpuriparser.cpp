#include "totpuriparser.h"
#include <stdexcept>

#include <QDebug>
#include <QUrlQuery>
#include <QUrl>

TotpUriParser::TotpUriParser(const QString &original)
{
    validateUrl(original);
    extractAccountName(original);
    extractQuery(original);
    validateQuery();
    extractQueryValues();
}

QString TotpUriParser::getAccountName()
{
    return user;
}

QString TotpUriParser::getSecret()
{
    return secret;
}

QString TotpUriParser::getIssuer()
{
    return issuer;
}

void TotpUriParser::validateUrl(QString original)
{
    if (original.isEmpty()){
        throw std::runtime_error("Empty url!");
    }

    int shemeLenght = original.lastIndexOf("/")+1;
    QString sheme = original.left(shemeLenght);
    qDebug() << "Detected sheme: " << sheme;

    int cmp = QString::compare("otpauth://totp/", sheme, Qt::CaseSensitive);
    if (cmp != 0) {
        throw std::runtime_error("Wrong sheme!");
    }
}

void TotpUriParser::extractQuery(QString original)
{
    query = original;
    int otpauthEnd = original.indexOf("?",0)+1;
    query.remove(0,otpauthEnd);
}

void TotpUriParser::validateQuery()
{
    if (query.isEmpty()){
        throw std::runtime_error("Empty query!");
    }
    if (!query.contains("secret")){
        throw std::runtime_error("NO secret!");
    }
    if (!query.contains("issuer")){
        throw std::runtime_error("NO issuer!");
    }
}

void TotpUriParser::extractAccountName(QString original)
{
    QUrl url(original);
    QString path = url.path();
    qDebug() << "Detected path: " << path;

    user = path.remove(0,1);
    qDebug() << "Detected user: " << user;
    if (user.isEmpty()){
        throw std::runtime_error("NO user!");
    }
}

void TotpUriParser::extractQueryValues()
{
    QUrlQuery url(query);
    secret = url.queryItemValue("secret");
    issuer = url.queryItemValue("issuer");

    qDebug() << "Detected issuer: " << issuer;
}
