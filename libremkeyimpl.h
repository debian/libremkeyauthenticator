#ifndef LIBREMKEYIMPL_H
#define LIBREMKEYIMPL_H

#include <memory>

#include "libremkeybase.h"

namespace nitrokey {
    class NitrokeyManager;
}

/**
 * @brief Actual implementation of LibremKeyBase interface,
 * that uses actual USB key.
 *
 * For detailed documentation see LibremKeyBase.
 */
class LibremKeyImpl : public LibremKeyBase
{
private:
    std::shared_ptr<nitrokey::NitrokeyManager> keyManager;

public:
    LibremKeyImpl(int logLevel);
    virtual bool connect() override;
    virtual bool isConnected() const override;
    virtual bool firstAuth(const std::string &adminPin, const std::string tempPassword) override;
    virtual void userAuth(const std::string &userPin, const std::string &tempPassword) override;
    virtual void writeTotpSlot(std::uint8_t slotNumber, const std::string &slotName,
                               const std::string &hexSecret, std::uint16_t timeWindow,
                               const std::string &temporaryPassword) override;

    virtual std::vector<TOTPSlot> getSlots() const override;
    virtual std::string getSlotName(std::uint8_t slot) const override;
};

#endif // LIBREMKEYIMPL_H
