#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>
#include <QMainWindow>

#include <zbar/QZBar.h>

class LibremKeyBase;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(std::unique_ptr<LibremKeyBase> libremkey, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onScreenshotClicked();
    void onDecodedText(const QString &);
    void onDecoded();

private:
    Ui::MainWindow *ui;
    std::unique_ptr<LibremKeyBase> libremKey;
    bool connected;
    zbar::QZBar scanner;
};

#endif // MAINWINDOW_H
